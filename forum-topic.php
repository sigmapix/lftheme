<?php include("inc/head.php"); ?>
<body class="bibliotheque">
<?php include("inc/header.php"); ?>
<div id="content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="title">
					<a href="#">
						<svg class="goback" preserveAspectRatio="xMinYMin meet" viewBox="0 0 18 16" class="arrows-icon"> <path class="left-arrow" d="M0.6,6.6l6-6c0.8-0.8,2-0.8,2.8,0c0.8,0.8,0.8,2,0,2.8L6.8,6H16c1.1,0,2,0.9,2,2s-0.9,2-2,2H6.8l2.6,2.6 c0.8,0.8,0.8,2,0,2.8C9,15.8,8.5,16,8,16s-1-0.2-1.4-0.6l-6-6C-0.2,8.6-0.2,7.4,0.6,6.6z"></path> </svg>
						Forum
					</a>
				</h1>
			</div>
			<div class="col-md-12">

				<div class="box-white containerBiblio subjectForum">
					<div class="item question">
						<div class="col-md-1 center">
							<a href="#" class="avatarPost">
								<img src="images/avatar.png" alt="" class="avatar">
							</a>
						</div>
						<div class="col-md-11 content">
							<h2>Microsoft Word 2015 : Les raccourcis utiles</h2>
							<div class="tags">
								<a href="#" style="background: #275897;color: #FFF"><img src="images/iconsmall-word.png" alt="">Microsoft Word 2015</a>
								<a href="#">Typo</a>
								<a href="#">Majuscule</a>
							</div>
							<div class="dateauthor">
								posée le 23 oct. 2015 à 12h39  par <a href="#">Thibaut L.</a>
							</div>
							<div class="message">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit: </p>

								<p>
									Sed do eiusmod tempor incididunt ut labore et dolore magna <br>
									Ccusantium doloremque laudantiu<br>
									Consectetur adipisicing elit, sed do eiusmod tempor
								</p>

								<p>Nemo enim ipsam voluptatem quia voluptas?  </p>

								<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum?</p>

								<p>Merci d'avance.</p>
							</div>
						</div>
						<a href="#" class="addFav inFav"></a> <!--Classe si "En favoris" -> .inFav-->
					</div>
					<div class="reponsesTitle">
						3 réponses
					</div>
					<div class="item anwser selected">
						<div class="col-md-1 center">
							<a href="#" class="avatarPost">
								<img src="images/nobody.png" alt="" class="avatar">
							</a>
						</div>
						<div class="col-md-11 content">
							<div class="dateauthor">
								<img src="images/resolu.png" alt=""> <a href="#">Thibaut L.</a> a répondu :<br>
								<span>reponse postée le 23 oct. 2015 à 12h39 </span>
							</div>
							<div class="message">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
								<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
								<p>Est-ce que c'est OK comme ça ?</p>
							</div>
						</div>
					</div>
					<div class="item anwser">
						<div class="col-md-1 center">
							<a href="#" class="avatarPost">
								<img src="images/nobody.png" alt="" class="avatar">
							</a>
						</div>
						<div class="col-md-11 content">
							<div class="dateauthor">
								<a href="#">Antony B.</a> a répondu :<br>
								<span>reponse postée le 23 oct. 2015 à 12h39 </span>
							</div>
							<div class="message">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
								<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
								<p>Est-ce que c'est OK comme ça ?</p>
							</div>
						</div>
					</div>
					<div class="item anwser last">
						<div class="col-md-1 center">
							<a href="#" class="avatarPost">
								<img src="images/nobody.png" alt="" class="avatar">
							</a>
						</div>
						<div class="col-md-11 content">
							<div class="dateauthor">
								<a href="#">Caroline S.</a> a répondu :<br>
								<span>reponse postée le 23 oct. 2015 à 12h39 </span>
							</div>
							<div class="message">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
								<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
								<p>Est-ce que c'est OK comme ça ?</p>
							</div>
						</div>
					</div>
					<div class="reponsesTitle">
						Poster une réponse
					</div>
					<div id="postanswer">
						<div class="col-md-1 center">
							<a href="#" class="avatarPost">
								<img src="images/avatar.png" alt="" class="avatar">
							</a>
						</div>
						<div class="col-md-10">
							<form>
								<div class="form-group">
									<textarea class="form-control" id="field-message" required></textarea>
								</div>
								<div class="right">
									<input checked="checked" id="notifyMe" name="notifyMe" type="checkbox" value="1">
									<label for="notifyMe">M'avertir par e-mail quand quelqu'un répond</label>
									<button type="submit" class="btn btn-primary">Poster ma réponse</button>
								</div>
							</form>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<?php include("inc/footer.php"); ?>

