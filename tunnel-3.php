<!DOCTYPE html>
<html lang="fr-FR" prefix="og: http://ogp.me/ns#" class="no-js">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://use.typekit.net/gqh0zbj.js"></script>
    <script>try {
            Typekit.load({async: true});
        } catch (e) {
        }</script>
    <!-- Styles -->
    <!-- End styles -->
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- Scripts -->
    <!--[if lt IE 9]>
    <script type='text/javascript' src='http://www.live-formation.com/wp-content/themes/LiveFormation/assets/libs/respond/dest/respond.min.js'></script>
    <script type='text/javascript' src='http://www.live-formation.com/wp-content/themes/LiveFormation/assets/libs/html5shiv/dist/html5shiv.min.js'></script>
    <![endif]-->
    <!-- End scripts -->
    <title>Live Formation</title>

    <link rel='stylesheet' id='bootstrap-css' href='http://www.live-formation.com/wp-content/themes/LiveFormation/assets/libs/bootstrap/dist/css/bootstrap.min.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css' href='http://www.live-formation.com/wp-content/themes/LiveFormation/css/style.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css' href='tunnel.css' type='text/css' media='all'/>
    <script type='text/javascript' src='http://www.live-formation.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
    <script type='text/javascript' src='http://www.live-formation.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
</head>

<body class="page">
<div id="page" class="site">
    <div class="site-inner">

        <header id="masthead" class="small">
            <div class="container">
                <a href="/" id="logo"> <img src="http://www.live-formation.com/wp-content/themes/LiveFormation/images/logo.png" alt=""></a>
            </div>
        </header>

        <div id="content" class="site-content">
            <div id="primary" class="content-area contact">
                <main id="main" class="site-main">

                    <div id="titleCat">
                        <div class="container">
                            <div class="col-md-10 center">
                                <h1>Inscription</h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore <br>et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                    ullamc</p>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <form action="#" method="post" class="">
                            <div class="col-md-8 formulaire">
                                <div role="form">

                                    <p><span class="titleform">Vos informations personnelles</span></p>
                                    <div class="contentForm">
                                        <div class="full">
                                            <span>Nom<i>*</i></span>
                                            <input type="text" name="nom" value="" size="40" class="" aria-required="true" aria-invalid="false">
                                        </div>
                                        <div class="full">
                                            <span>Prénom<i>*</i></span>
                                            <input type="text" name="prenom" value="" size="40" class="" aria-required="true" aria-invalid="false">
                                        </div>
                                        <div class="full">
                                            <span>Adresse<i>*</i></span>
                                            <input type="email" name="adresse" value="" size="40" class="" aria-required="true" aria-invalid="false">
                                        </div>
                                        <div class="full">
                                            <span>Code postal<i>*</i></span>
                                            <input type="email" name="cp" value="" size="40" class="" aria-required="true" aria-invalid="false">
                                        </div>
                                        <div class="full">
                                            <label><input type="checkbox" id="" value="cgv"> Je certifie avoir plus de 18 ans</label><br>
                                            <label><input type="checkbox" id="" value="5"> J'accepte les <a href="#">confitions générales de ventes</a></label><br>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <input type="submit" value="Suivant" class="gonext">
                        </form>

                    </div>
                </main>

            </div>

            <footer id="colophon" class="site-footer" role="contentinfo">
                <a href="/" class="logo"></a>
                <p>© 2016 Tous droits réservés.</p>
                <p><a href="#">CGV</a> • <a href="#">Mentions</a> • <a href="#">Blog</a> • <a href="#">Contact</a></p>
                <!--<p class="social">
                    <a href="#" class="facebook"></a> +
                    <a href="#" class="googleplus"></a> +
                    <a href="#" class="twitter"></a>
                </p>-->
            </footer>
        </div>
    </div>

</body>
</html>
