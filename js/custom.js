
jQuery(document).ready(function($) {

    $('.sliderStats').bxSlider({
        moveSlides : 3,
        slideWidth: 100,
        minSlides: 3,
        maxSlides: 3,
        slideMargin: 0,
        pager: false,
        nextText: "",
        prevText: "",
        onSliderLoad : function(){
            $(".miniStats .bx-viewport").css("width","99%");
        }
    });

    $("#contacterFormateur").hover(
        function(){
            $(this).css("right","250px");
        },
        function(){
            $(this).css("right",0);
        }
    );

    /* TOOLBOX */

    $("#toolbox").hover(function(){
       $(this).toggleClass("open");
       $("body").toggleClass("noScroll");
    });

    var lieach;

    $("#toolbox .list li").each(function(){
        lieach = $(this);
        $(lieach).height($(lieach).height());
        $(lieach).find(".tempspasse, .delete").css("lineHeight",$(lieach).height() + "px");
    });

    $("#toolbox .list:not(.current)").addClass("hide");

    $("#toolbox .sidebar a").click(function(e){
        e.preventDefault();
        $("#toolbox .sidebar a").removeClass("current"); $(this).addClass("current");
        $("#toolbox .content .list").addClass("hide").removeClass("current");
        $("#toolbox .content .list").eq($(this).index()).addClass("current").removeClass("hide");
    });

    $("#toolbox .list li .delete").click(function(e){
        e.preventDefault();
        $(this).parents("li").animate({ right: "-100%", opacity: 0, height : 0, padding : 0 }, 300, function() { $(this).hide();});
    });

    $(".form-control").focusin(function() {
        $("label[for='"+$(this).attr("id")+"']").css("color", "#27ae60");
    });

    $(".form-control").focusout(function() {
        $("label[for='"+$(this).attr("id")+"']").css("color","#747c83");
    });

    $(".une-formation .actions .stats").click(function(){
        $("#statsFormation").modal('show');
    });

    $('#statsFormation').on('show.bs.modal', function (e) {
        setTimeout(function(){

            var barChartData = {
                labels : ["S11","S12","S13","S14","S15","S16","S17","S18"],
                datasets : [
                    {
                        strokeColor : "rgba(220,220,220,0)",
                        pointHighlightFill : "#fff",
                        pointHighlightStroke : "rgba(220,220,220,1)",
                        data : [2,1,8,7,4,8,3,6],
                        scaleShowGridLines : false,
                        bezierCurve : false,
                        fillColor : "#fff",
                        scaleGridLineColor: "rgba(255, 255, 255, 0.1)"
                    }
                ]
            };

            var ctx = document.getElementById("canvasStatsFormation").getContext("2d");
            window.myBar = new Chart(ctx).Bar(barChartData, {
                responsive: false,
                pointDot: false,
                scaleShowVerticalLines: false,
                datasetStrokeWidth : 4,
                bezierCurveTension : 0,
                scaleGridLineColor : "rgba(255,255,255,.15)",
                scaleFontColor: "#fff",
                scaleFontFamily : "proxima-nova-soft",
                pointLabelFontFamily : "proxima-nova-soft",
                scaleLabel:	function(label){return label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "h";},
                scaleLineColor: "rgba(255, 255, 255, 0.1)",
                showTooltips: false
            });

            var doughnutData = [
                {
                    value: 85,
                    color:"#27AE60"
                },
                {
                    value: 100-85,
                    color:"#EFF1F5"
                }
            ];

            var ctx = document.getElementById("chart-area").getContext("2d");
            window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive : false,showTooltips: false, animationSteps : 100, animationEasing : "easeOutExpo"});

        },300);

    });

    $(".moncompte .editinfobtn").click(function(e){
        e.preventDefault();
        $(".showinfo").addClass("hide");
        $(".editinfo").removeClass("hide");
    });

    $(".moncompte .submit, .moncompte .cancel").click(function(e){
        e.preventDefault();
        $(".showinfo").removeClass("hide");
        $(".editinfo").addClass("hide");
    });

    $(".moncompte .editinfo .uploader .button").click(function(e){
        e.preventDefault();
        $(".moncompte .editinfo .uploader .avatars").fadeIn("fast");
    });

});

/* DEV MODE ONLY. Merci de supprimer */

$(window).load(function(){
    $('#posttopic').modal('show');
});

/*************************************/