<?php include("inc/head.php"); ?>
<body class="bibliotheque">
<?php include("inc/header.php"); ?>
<div id="content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="title">Bibliothèque</h1>
				<p class="desc">
					<a href="#" class="contactformateur"><img src="images/chatformateur.png" alt=""> Contacter le formateur</a> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.
				</p>
			</div>
			<div class="col-md-9">

				<div class="filterstop">
					<a href="#" class="active">Tous <span>1059</span></a>
					<a href="#">Excerices <span>150</span></a>
					<a href="#">Astuces <span>650</span></a>
					<a href="#">Synthèses <span>38</span></a>
					<a href="#" class="right favoris">Favoris <span>2</span></a>
				</div>

				<div class="box-white containerBiblio">
					<div class="item">
						<div class="col-md-8 content">
							<h2><strong>Astuce •</strong> Microsoft Word 2015 : Les raccourcis utiles</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
							<div class="tags">
								<a href="#" style="background: #275897;color: #FFF"><img src="images/iconsmall-word.png" alt="">Microsoft Word 2015</a>
								<a href="#">Typo</a>
								<a href="#">Majuscule</a>
							</div>
						</div>
						<div class="col-md-4 infos">
							<div class="col-md-6 nopad">
								<a href="#" class="download">
									<span class="icon"></span>
									Télécharger
								</a>
							</div>
							<div class="col-md-6 nopad">
								<span class="niveau">
									<span class="icon debutant"></span>
									Débutant
								</span>
							</div>
						</div>
						<a href="#" class="addFav"></a> <!--Classe si "En favoris" -> .inFav-->
					</div>
					<div class="item">
						<div class="col-md-8 content">
							<h2><strong>Astuce •</strong> Microsoft Word 2015 : Les raccourcis utiles</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
							<div class="tags">
								<a href="#" style="background: #a23537;color: #FFF"><img src="images/iconsmall-access.png" alt="">Microsoft Access 2015</a>
								<a href="#">Fusion</a>
								<a href="#">Module</a>
							</div>
						</div>
						<div class="col-md-4 infos">
							<div class="col-md-6 nopad">
								<a href="#" class="download">
									<span class="icon"></span>
									Télécharger
								</a>
							</div>
							<div class="col-md-6 nopad">
								<span class="niveau">
									<span class="icon expert"></span>
									Expert
								</span>
							</div>
						</div>
						<a href="#" class="addFav"></a>
					</div>
					<div class="item">
						<div class="col-md-8 content">
							<h2><strong>Astuce •</strong> Microsoft Word 2015 : Les raccourcis utiles</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
							<div class="tags">
								<a href="#" style="background: #1d7145;color: #FFF"><img src="images/iconsmall-excel.png" alt="">Microsoft Excel 2015</a>
								<a href="#">Tableau</a>
								<a href="#">Repertoire</a>
							</div>
						</div>
						<div class="col-md-4 infos">
							<div class="col-md-6 nopad">
								<a href="#" class="download">
									<span class="icon"></span>
									Télécharger
								</a>
							</div>
							<div class="col-md-6 nopad">
								<span class="niveau">
									<span class="icon intermediaire"></span>
									Intermédiaire
								</span>
							</div>
						</div>
						<a href="#" class="addFav inFav"></a>
					</div>
					<div class="item">
						<div class="col-md-8 content">
							<h2><strong>Astuce •</strong> Microsoft Word 2015 : Les raccourcis utiles</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
							<div class="tags">
								<a href="#" style="background: #275897;color: #FFF"><img src="images/iconsmall-word.png" alt="">Microsoft Word 2015</a>
								<a href="#">Typo</a>
								<a href="#">Majuscule</a>
							</div>
						</div>
						<div class="col-md-4 infos">
							<div class="col-md-6 nopad">
								<a href="#" class="download">
									<span class="icon"></span>
									Télécharger
								</a>
							</div>
							<div class="col-md-6 nopad">
								<span class="niveau">
									<span class="icon intermediaire"></span>
									Intermédiaire
								</span>
							</div>
						</div>
						<a href="#" class="addFav"></a>
					</div>
					<div class="item">
						<div class="col-md-8 content">
							<h2><strong>Astuce •</strong> Microsoft Word 2015 : Les raccourcis utiles</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
							<div class="tags"> 	<a href="#" style="background: #a23537;color: #FFF"><img src="images/iconsmall-access.png" alt="">Microsoft Access 2015</a> 	<a href="#">Fusion</a> 	<a href="#">Module</a> </div>
						</div>
						<div class="col-md-4 infos">
							<div class="col-md-6 nopad">
								<a href="#" class="download">
									<span class="icon"></span>
									Télécharger
								</a>
							</div>
							<div class="col-md-6 nopad">
								<span class="niveau">
									<span class="icon debutant"></span>
									Débutant
								</span>
							</div>
						</div>
						<a href="#" class="addFav"></a>
					</div>
					<div class="item">
						<div class="col-md-8 content">
							<h2><strong>Astuce •</strong> Microsoft Word 2015 : Les raccourcis utiles</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
							<div class="tags"> 	<a href="#" style="background: #275897;color: #FFF"><img src="images/iconsmall-word.png" alt="">Microsoft Word 2015</a> 	<a href="#">Typo</a> 	<a href="#">Majuscule</a> </div>
						</div>
						<div class="col-md-4 infos">
							<div class="col-md-6 nopad">
								<a href="#" class="download">
									<span class="icon"></span>
									Télécharger
								</a>
							</div>
							<div class="col-md-6 nopad">
								<span class="niveau">
									<span class="icon debutant"></span>
									Débutant
								</span>
							</div>
						</div>
						<a href="#" class="addFav"></a>
					</div>
					<div class="item">
						<div class="col-md-8 content">
							<h2><strong>Astuce •</strong> Microsoft Word 2015 : Les raccourcis utiles</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
							<div class="tags"> 	<a href="#" style="background: #275897;color: #FFF"><img src="images/iconsmall-word.png" alt="">Microsoft Word 2015</a> 	<a href="#">Typo</a> 	<a href="#">Majuscule</a> </div>
						</div>
						<div class="col-md-4 infos">
							<div class="col-md-6 nopad">
								<a href="#" class="download">
									<span class="icon"></span>
									Télécharger
								</a>
							</div>
							<div class="col-md-6 nopad">
								<span class="niveau">
									<span class="icon debutant"></span>
									Débutant
								</span>
							</div>
						</div>
						<a href="#" class="addFav"></a>
					</div>
					<div class="item">
						<div class="col-md-8 content">
							<h2><strong>Astuce •</strong> Microsoft Word 2015 : Les raccourcis utiles</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
							<div class="tags"> 	<a href="#" style="background: #a23537;color: #FFF"><img src="images/iconsmall-access.png" alt="">Microsoft Access 2015</a> 	<a href="#">Fusion</a> 	<a href="#">Module</a> </div>
						</div>
						<div class="col-md-4 infos">
							<div class="col-md-6 nopad">
								<a href="#" class="download">
									<span class="icon"></span>
									Télécharger
								</a>
							</div>
							<div class="col-md-6 nopad">
								<span class="niveau">
									<span class="icon expert"></span>
									Expert
								</span>
							</div>
						</div>
						<a href="#" class="addFav"></a>
					</div>

				</div>

				<div class="pagination">
					<a href="#" class="prec">Prec</a>
					<a href="#" class="pagenb">1</a>
					<a href="#" class="pagenb">2</a>
					<a href="#" class="pagenb">3</a>
					<a href="#" class="pagenb">4</a>
					<a href="#" class="pagenb current">5</a>
					<a href="#" class="pagenb">6</a>
					<a href="#" class="pagenb">7</a>
					<a href="#" class="pagenb">8</a>
					<a href="#" class="suiv">Suiv</a>
				</div>

			</div>
			<div class="col-md-3 filtres">
				<div class="search box-white">
					<form action="">
						<input type="text" class="searchQ" placeholder="Rechercher un sujet..."/>
						<button type="submit" class="submit"></button>
					</form>
				</div>
				<div class="by-App box-white">
					<strong class="title">Applications</strong>
					<div class="mainCheck">
						<input type="checkbox" checked id="MicrosoftOffice"><label for="MicrosoftOffice">Microsoft Office <img src="images/smallicon-office.png" alt=""></label>
					</div>
					<div class="sousCheck">
						<input type="checkbox" id="Word"><label for="Word">Word <img src="images/smallicon-word.png" alt=""></label>
						<input type="checkbox" checked id="Excel"><label for="Excel">Excel <img src="images/smallicon-excel.png" alt=""></label>
						<input type="checkbox" id="Outlook"><label for="Outlook">Outlook <img src="images/smallicon-outlook.png" alt=""></label>
						<input type="checkbox" id="Acces"><label for="Acces">Acces <img src="images/smallicon-access.png" alt=""></label>
						<input type="checkbox" id="Powerpoint"><label for="Powerpoint">Powerpoint <img src="images/smallicon-powerpoint.png" alt=""></label>
					</div>
				</div>
				<div class="by-Version box-white">
					<strong class="title">Versions</strong>
					<div class="mainCheck">
						<input type="checkbox" checked id="Excel"><label for="Excel">Microsoft Excel</label>
					</div>
					<div class="sousCheck versions">
						<a href="#">2007</a> <a href="#">2010</a> <a href="#" class="selected">2013</a>
					</div>
				</div>
				<div class="by-Metier box-white">
					<strong class="title">Catégorie métier</strong>
					<input type="checkbox" checked id="Management"><label for="Management">Management</label>
					<input type="checkbox" id="Secrétariat"><label for="Secrétariat">Secrétariat</label>
					<input type="checkbox" id="Commercial"><label for="Commercial">Commercial</label>
					<input type="checkbox" id="Assistanat"><label for="Assistanat">Assistanat</label>
				</div>
				<div class="by-Difficulte box-white">
					<strong class="title">Niveau</strong>
					<input type="checkbox" id="Debutant"><label for="Debutant">Débutant <span class="icon Debutant"></span></label>
					<input type="checkbox" id="Intermediaire"><label for="Intermediaire">Intermédiaire <span class="icon Intermediaire"></span></label>
					<input type="checkbox" id="Expert"><label for="Expert">Expert <span class="icon Expert"></span></label>
				</div>
				<div class="clear"></div>
				<a href="#" class="save"><span class="icon"></span> Sauvegarder la recherche</a>
				<a href="#" class="reset"><span class="icon"></span>Ré-initialiser les filtres</a>
			</div>
		</div>
	</div>
</div>
<?php include("inc/footer.php"); ?>

