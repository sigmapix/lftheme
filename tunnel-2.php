<!DOCTYPE html>
<html lang="fr-FR" prefix="og: http://ogp.me/ns#" class="no-js">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://use.typekit.net/gqh0zbj.js"></script>
    <script>try {
            Typekit.load({async: true});
        } catch (e) {
        }</script>
    <!-- Styles -->
    <!-- End styles -->
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- Scripts -->
    <!--[if lt IE 9]>
    <script type='text/javascript' src='http://www.live-formation.com/wp-content/themes/LiveFormation/assets/libs/respond/dest/respond.min.js'></script>
    <script type='text/javascript' src='http://www.live-formation.com/wp-content/themes/LiveFormation/assets/libs/html5shiv/dist/html5shiv.min.js'></script>
    <![endif]-->
    <!-- End scripts -->
    <title>Live Formation</title>

    <link rel='stylesheet' id='bootstrap-css' href='http://www.live-formation.com/wp-content/themes/LiveFormation/assets/libs/bootstrap/dist/css/bootstrap.min.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css' href='http://www.live-formation.com/wp-content/themes/LiveFormation/css/style.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css' href='tunnel.css' type='text/css' media='all'/>
    <script type='text/javascript' src='http://www.live-formation.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
    <script type='text/javascript' src='http://www.live-formation.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
</head>

<body class="page">
<div id="page" class="site">
    <div class="site-inner">

        <header id="masthead" class="small">
            <div class="container">
                <a href="/" id="logo"> <img src="http://www.live-formation.com/wp-content/themes/LiveFormation/images/logo.png" alt=""></a>
            </div>
        </header>

        <div id="content" class="site-content">
            <div id="primary" class="content-area contact">
                <main id="main" class="site-main">

                    <div id="titleCat">
                        <div class="container">
                            <div class="col-md-10 center">
                                <h1>Connectez-vous</h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore <br>et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                    ullamc</p>
                            </div>
                        </div>
                    </div>

                    <div class="container logins">

                        <form action="#" method="post" class="">
                            <div class="col-md-5 formulaire left">
                                <div role="form">

                                    <p><span class="titleform">Nouvelle inscription</span></p>
                                    <div class="contentForm">

                                        <div class="full">
                                            <span>Adresse e-mail<i>*</i></span>
                                            <input type="email" name="email" value="" size="40" class="" aria-required="true" aria-invalid="false">
                                        </div>
                                        <div class="full">
                                            <span>Mot de passe<i>*</i></span>
                                            <input type="password" name="password" value="" size="40" class="" aria-required="true" aria-invalid="false">
                                        </div>
                                        <div class="full">
                                            <span>Confirmer le mot de passe<i>*</i></span>
                                            <input type="password" name="password2" value="" size="40" class="" aria-required="true" aria-invalid="false">
                                        </div>
                                        <br>
                                        <input type="submit" value="Suivant" class="nomargin">
                                    </div>
                                </div>
                            </div>
                        </form>

                        <form action="#" method="post" class="">
                            <div class="col-md-5 formulaire right">
                                <div role="form">

                                    <p><span class="titleform">Déjà client</span></p>
                                    <div class="contentForm">

                                        <div class="full">
                                            <span>Adresse e-mail<i>*</i></span>
                                            <input type="email" name="email" value="" size="40" class="" aria-required="true" aria-invalid="false">
                                        </div>
                                        <div class="full">
                                            <span>Mot de passe<i>*</i></span>
                                            <input type="password" name="password" value="" size="40" class="" aria-required="true" aria-invalid="false">
                                        </div>
                                        <br>
                                        <input type="submit" value="Connexion" class="nomargin">
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </main>


            </div>

            <footer id="colophon" class="site-footer" role="contentinfo">
                <a href="/" class="logo"></a>
                <p>© 2016 Tous droits réservés.</p>
                <p><a href="#">CGV</a> • <a href="#">Mentions</a> • <a href="#">Blog</a> • <a href="#">Contact</a></p>
                <!--<p class="social">
                    <a href="#" class="facebook"></a> +
                    <a href="#" class="googleplus"></a> +
                    <a href="#" class="twitter"></a>
                </p>-->
            </footer>
        </div>
    </div>

</body>
</html>
