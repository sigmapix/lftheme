<!DOCTYPE html>
<html lang="fr-FR" prefix="og: http://ogp.me/ns#" class="no-js">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://use.typekit.net/gqh0zbj.js"></script>
    <script>try {
            Typekit.load({async: true});
        } catch (e) {
        }</script>
    <!-- Styles -->
    <!-- End styles -->
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- Scripts -->
    <!--[if lt IE 9]>
    <script type='text/javascript' src='http://www.live-formation.com/wp-content/themes/LiveFormation/assets/libs/respond/dest/respond.min.js'></script>
    <script type='text/javascript' src='http://www.live-formation.com/wp-content/themes/LiveFormation/assets/libs/html5shiv/dist/html5shiv.min.js'></script>
    <![endif]-->
    <!-- End scripts -->
    <title>Live Formation</title>

    <link rel='stylesheet' id='bootstrap-css' href='http://www.live-formation.com/wp-content/themes/LiveFormation/assets/libs/bootstrap/dist/css/bootstrap.min.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css' href='http://www.live-formation.com/wp-content/themes/LiveFormation/css/style.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css' href='tunnel.css' type='text/css' media='all'/>
    <script type='text/javascript' src='http://www.live-formation.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
    <script type='text/javascript' src='http://www.live-formation.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
</head>

<body class="page">
<div id="page" class="site">
    <div class="site-inner">

        <header id="masthead" class="small">
            <div class="container">
                <a href="/" id="logo"> <img src="http://www.live-formation.com/wp-content/themes/LiveFormation/images/logo.png" alt=""></a>
            </div>
        </header>

        <div id="content" class="site-content">
            <div id="primary" class="content-area contact">
                <main id="main" class="site-main">

                    <div id="titleCat" class="validation">
                        <div class="container">
                            <div class="col-md-10 center">
                                <h1>Validation</h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore <br>et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                    ullamc</p>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="col-md-8 formulaire">
                            <div role="form">

                                <p><span class="titleform">Récapitulatif</span></p>
                                <div class="contentForm recap">
                                    <strong>Vous avez choisis l'offre : <strong>Pack Premium</strong></strong><br>
                                    <p>1 Formation au choix avec certification TOSA</p>
                                    <p>Accompagnement de 12 mois par un formateur</p><br>
                                    <strong>J'ai choisi le logiciel</strong>
                                    <p>Microsoft Word 2020</p><br>
                                    <strong>Montant total à régler</strong>
                                    <p>
                                        <span class="price">
                                            235<sup>€ <span>TTC</span></sup>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br><br><br>

                    <div class="container">
                        <form action="#" method="post" class="">
                            <div class="col-md-8 formulaire noshadow">
                                <div role="form">
                                    <p><span class="titleform">Je choisis mon mode de paiement</span></p>
                                    <div class="contentForm">
                                        <div class="semi">
                                            <strong>Je souhaite paiement en :</strong><br>
                                            <select name="nbfois" id="">
                                                <option value="2">2 fois sans frais</option>
                                                <option value="4">4 fois sans frais</option>
                                            </select>
                                        </div>
                                        <div class="semi">
                                            <br>
                                            <p>(soit 2 mensualités de 36,55€)</p>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="full moyenpaid">
                                            <a href="#" id="cb_"><img src="images/cb.jpg" alt=""></a>
                                            <a href="#" id="visa_"><img src="images/visa.jpg" alt=""></a>
                                            <a href="#" id="master_"><img src="images/master.jpg" alt=""></a>
                                            <a href="#" id="maestro_"><img src="images/maestro.jpg" alt=""></a>
                                        </div>
                                        <p class="small">
                                            En passant commande, je renonce expressément à mon droit de rétractation. <br>(Code de la consommation - Article L121-21-8)
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <input type="submit" value="Valider" class="gonext green">
                        </form>

                    </div>

                </main>

            </div>

            <footer id="colophon" class="site-footer" role="contentinfo">
                <a href="/" class="logo"></a>
                <p>© 2016 Tous droits réservés.</p>
                <p><a href="#">CGV</a> • <a href="#">Mentions</a> • <a href="#">Blog</a> • <a href="#">Contact</a></p>
                <!--<p class="social">
                    <a href="#" class="facebook"></a> +
                    <a href="#" class="googleplus"></a> +
                    <a href="#" class="twitter"></a>
                </p>-->
            </footer>
        </div>
    </div>

</body>
</html>
