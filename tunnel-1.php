<!DOCTYPE html>
<html lang="fr-FR" prefix="og: http://ogp.me/ns#" class="no-js">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://use.typekit.net/gqh0zbj.js"></script>
    <script>try {
            Typekit.load({async: true});
        } catch (e) {
        }</script>
    <!-- Styles -->
    <!-- End styles -->
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- Scripts -->
    <!--[if lt IE 9]>
    <script type='text/javascript' src='http://www.live-formation.com/wp-content/themes/LiveFormation/assets/libs/respond/dest/respond.min.js'></script>
    <script type='text/javascript' src='http://www.live-formation.com/wp-content/themes/LiveFormation/assets/libs/html5shiv/dist/html5shiv.min.js'></script>
    <![endif]-->
    <!-- End scripts -->
    <title>Live Formation</title>

    <link rel='stylesheet' id='bootstrap-css' href='http://www.live-formation.com/wp-content/themes/LiveFormation/assets/libs/bootstrap/dist/css/bootstrap.min.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css' href='http://www.live-formation.com/wp-content/themes/LiveFormation/css/style.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css' href='tunnel.css' type='text/css' media='all'/>
    <script type='text/javascript' src='http://www.live-formation.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
    <script type='text/javascript' src='http://www.live-formation.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
</head>

<body class="page">
<div id="page" class="site">
    <div class="site-inner">

        <header id="masthead" class="small">
            <div class="container">
                <a href="/" id="logo"> <img src="http://www.live-formation.com/wp-content/themes/LiveFormation/images/logo.png" alt=""></a>
            </div>
        </header>

        <div id="content" class="site-content">
            <div id="primary" class="content-area contact">
                <main id="main" class="site-main">

                    <div id="titleCat">
                        <div class="container">
                            <div class="col-md-10 center">
                                <h1>Vous avez choisi le pack "Premium"</h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                                    cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <form action="#" method="post" class="">
                            <div class="col-md-8 formulaire">
                                <div role="form">

                                    <p><span class="titleform">Je précise mon besoin</span></p>
                                    <div class="contentForm">
                                        <div class="full">
                                            <span><strong>Je souhaite une formation sur le(s) logiciel(s) suivant(s) :</strong></span> <br>
                                        </div>

                                        <div class="line first">
                                            <div class="tier">
                                                <span><strong>Application</strong></span>
                                            </div>
                                            <div class="tier version">
                                                <span><strong>Version <span class="star">*</span></strong></span>
                                            </div>
                                            <div class="tier">
                                                <span><strong>Certification</strong></span>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="line">
                                            <div class="tier">
                                                <label><input type="checkbox" id="" value="1"> Microsoft Windows</label>
                                            </div>
                                            <div class="tier version">
                                                <select name="version" id="">
                                                    <option value=""></option>
                                                    <option value="2000">2000</option>
                                                    <option value="2001">2001</option>
                                                </select>
                                            </div>
                                            <div class="tier">
                                                <label><input type="checkbox" id="" value="2"> Oui</label>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="line">
                                            <div class="tier">
                                                <label><input type="checkbox" id="" value="1"> Microsoft Word</label>
                                            </div>
                                            <div class="tier version">
                                                <select name="version" id="">
                                                    <option value=""></option>
                                                    <option value="2000">2000</option>
                                                    <option value="2001">2001</option>
                                                </select>
                                            </div>
                                            <div class="tier">
                                                <label><input type="checkbox" id="" value="2"> Oui</label>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="line">
                                            <div class="tier">
                                                <label><input type="checkbox" id="" value="1"> Microsoft Excel</label>
                                            </div>
                                            <div class="tier version">
                                                <select name="version" id="">
                                                    <option value=""></option>
                                                    <option value="2000">2000</option>
                                                    <option value="2001">2001</option>
                                                </select>
                                            </div>
                                            <div class="tier">
                                                <label><input type="checkbox" id="" value="2"> Oui</label>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="line">
                                            <div class="tier">
                                                <label><input type="checkbox" id="" value="1"> Microsoft Access</label>
                                            </div>
                                            <div class="tier version">
                                                <select name="version" id="">
                                                    <option value=""></option>
                                                    <option value="2000">2000</option>
                                                    <option value="2001">2001</option>
                                                </select>
                                            </div>
                                            <div class="tier">
                                                <label><input type="checkbox" id="" value="2"> Oui</label>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="line">
                                            <div class="tier">
                                                <label><input type="checkbox" id="" value="1"> Microsoft Word</label>
                                            </div>
                                            <div class="tier version">
                                                <select name="version" id="">
                                                    <option value=""></option>
                                                    <option value="2000">2000</option>
                                                    <option value="2001">2001</option>
                                                </select>
                                            </div>
                                            <div class="tier">
                                                <label><input type="checkbox" id="" value="2"> Oui</label>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="line">
                                            <div class="tier">
                                                <label><input type="checkbox" id="" value="1"> Microsoft Word</label>
                                            </div>
                                            <div class="tier version">
                                                <select name="version" id="">
                                                    <option value=""></option>
                                                    <option value="2000">2000</option>
                                                    <option value="2001">2001</option>
                                                </select>
                                            </div>
                                            <div class="tier">
                                                <label><input type="checkbox" id="" value="2"> Oui</label>
                                            </div>
                                        </div>
                                        <div class="clear"></div>

                                        <div id="priceme">
                                            <strong>Prix à payer</strong>
                                            <span class="price">
                                                235<sup>€ <span>TTC</span></sup>
                                            </span>
                                        </div>
                                        <br>
                                        <p style="text-align: center"><span class="star">*</span> <a class="howknow" href="#">Comment connaitre sa version ?</a></p>
                                    </div>
                                </div>
                            </div>
                            <input type="submit" value="Suivant" class="gonext">
                        </form>

                    </div>
                </main>

            </div>

            <footer id="colophon" class="site-footer" role="contentinfo">
                <a href="/" class="logo"></a>
                <p>© 2016 Tous droits réservés.</p>
                <p><a href="#">CGV</a> • <a href="#">Mentions</a> • <a href="#">Blog</a> • <a href="#">Contact</a></p>
                <!--<p class="social">
                    <a href="#" class="facebook"></a> +
                    <a href="#" class="googleplus"></a> +
                    <a href="#" class="twitter"></a>
                </p>-->
            </footer>
        </div>
    </div>

</body>
</html>
