<?php include("inc/head.php"); ?>
    <body class="moncompte">
<?php include("inc/header.php"); ?>
    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title">
                        Mon compte
                    </h1>
                </div>
                <div class="col-md-9">

                    <div class="box-white smallPad showinfo">
                        <div class="line">
                            <label for="form_avatar" class="col-md-3">Avatar</label>
                            <span class="field col-md-9"><img src="images/avatar.png" alt="" class="avatar"></span>
                        </div>
                        <div class="line">
                            <label for="form_prenom" class="col-md-3">Prénom</label>
                            <span class="field col-md-9">Thibaut</span>
                        </div>
                        <div class="line">
                            <label for="form_nom" class="col-md-3">Nom</label>
                            <span class="field col-md-9">Lamanthe</span>
                        </div>
                        <div class="line">
                            <label for="form_email" class="col-md-3">Adresse e-mail</label>
                            <span class="field col-md-9">thibaut@mikii.fr</span>
                        </div>
                        <div class="line">
                            <label for="form_mdp" class="col-md-3">Mot de passe</label>
                            <span class="field col-md-9">••••••••••••</span>
                        </div>
                        <div class="line">
                            <label for="form_societe" class="col-md-3">Société</label>
                            <span class="field col-md-9">SARL MIKII</span>
                        </div>
                        <div class="line">
                            <label for="form_adresse" class="col-md-3">Adresse postale</label>
                            <span class="field col-md-9">7 Rue de la République</span>
                        </div>
                        <div class="line">
                            <label for="form_cp" class="col-md-3">Code postal</label>
                            <span class="field col-md-9">45000</span>
                        </div>
                        <div class="line">
                            <label for="form_ville" class="col-md-3">Ville</label>
                            <span class="field col-md-9">Orléans</span>
                        </div>
                        <div class="spacer"></div>
                        <a href="#" class="btn btn-primary editinfobtn">Éditer mes informations</a>
                    </div>

                    <div class="box-white smallPad editinfo hide">
                        <form action="">
                            <div class="line">
                                <label for="person_profile_photo" class="col-md-3">Avatar</label>
                                <div class="field col-md-9">
                                    <div class="uploader" id="uniform-person_profile_photo">
                                        <img src="images/avatar.png" alt="" class="avatar">
                                        <span class="button" style="-webkit-user-select: none;">Choisir un avatar</span>
                                        <div class="avatars">
                                            <a href="#" class="current"><img src="images/avatar.png" alt="" class="avatar"></a>
                                            <a href="#"><img src="images/avatar.png" alt="" class="avatar"></a>
                                            <a href="#"><img src="images/avatar.png" alt="" class="avatar"></a>
                                            <a href="#"><img src="images/avatar.png" alt="" class="avatar"></a>
                                            <a href="#"><img src="images/avatar.png" alt="" class="avatar"></a>
                                            <a href="#"><img src="images/avatar.png" alt="" class="avatar"></a>
                                            <a href="#"><img src="images/avatar.png" alt="" class="avatar"></a>
                                            <a href="#"><img src="images/avatar.png" alt="" class="avatar"></a>
                                            <a href="#"><img src="images/avatar.png" alt="" class="avatar"></a>
                                            <a href="#"><img src="images/avatar.png" alt="" class="avatar"></a>
                                            <a href="#"><img src="images/avatar.png" alt="" class="avatar"></a>
                                            <a href="#"><img src="images/avatar.png" alt="" class="avatar"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="line">
                                <label for="form_prenom" class="col-md-3">Prénom</label>
                            <span class="field col-md-9">
                                <input id="form_avatar" name="form_avatar" type="text" value="Thibaut"/>
                            </span>
                            </div>
                            <div class="line">
                                <label for="form_nom" class="col-md-3">Nom</label>
                            <span class="field col-md-9">
                                <input id="form_avatar" name="form_avatar" type="text" value="Lamanthe"/>
                            </span>
                            </div>
                            <div class="line">
                                <label for="form_email" class="col-md-3">Adresse e-mail</label>
                            <span class="field col-md-9">
                                <input id="form_avatar" name="form_avatar" type="email" value="thibaut@mikii.fr"/>
                            </span>
                            </div>
                            <div class="line">
                                <label for="form_mdp" class="col-md-3">Mot de passe</label>
                            <span class="field col-md-9">
                                <input id="form_avatar" name="form_avatar" type="password" value="••••••••••••"/>
                            </span>
                            </div>
                            <div class="line">
                                <label for="form_societe" class="col-md-3">Société</label>
                            <span class="field col-md-9">
                                <input id="form_avatar" name="form_avatar" type="text" value="SARL MIKII"/>
                            </span>
                            </div>
                            <div class="line">
                                <label for="form_adresse" class="col-md-3">Adresse postale</label>
                            <span class="field col-md-9">
                                <input id="form_avatar" name="form_avatar" type="text" value="7 Rue de la République"/>
                            </span>
                            </div>
                            <div class="line">
                                <label for="form_cp" class="col-md-3">Code postal</label>
                            <span class="field col-md-9">
                                <input id="form_avatar" name="form_avatar" type="text" value="45000"/>
                            </span>
                            </div>
                            <div class="line">
                                <label for="form_ville" class="col-md-3">Ville</label>
                                <span class="field col-md-9">
                                    <input id="form_avatar" name="form_avatar" type="text" value="Orléans"/>
                                </span>
                            </div>
                            <div class="line">
                                <label for="form_select" class="col-md-3">Select</label>
                                <span class="field col-md-9">
                                    <select name="form_select" id="form_select">
                                        <option value="Option 1">Option 1</option>
                                        <option value="Option 1">Option 2</option>
                                    </select>
                                </span>
                            </div>
                            <div class="line">
                                <label for="form_select2" class="col-md-3">Select multiple</label>
                                <span class="field col-md-9">
                                    <select multiple name="form_select2" id="form_select2">
                                        <option value="Option 1">Option 1</option>
                                        <option value="Option 1">Option 2</option>
                                        <option value="Option 1">Option 3</option>
                                        <option value="Option 1">Option 4</option>
                                    </select>
                                </span>
                            </div>
                            <div class="line">
                                <label for="form_choose_check" class="col-md-3">Checkboxes</label>
                                <span class="field col-md-9">
                                    <input type="checkbox" name="group1" value="Milk" id="choo_1"> <label for="choo_1">Choix 1</label>
                                    <input type="checkbox" name="group1" value="Milk" id="choo_2"> <label for="choo_2">Choix 2</label>
                                </span>
                            </div>
                            <div class="line">
                                <label for="form_choose" class="col-md-3">Radio btn</label>
                                <span class="field col-md-9">
                                    <input type="radio" name="group1" value="Milk" id="choo1"> <label for="choo1">Choix 1</label>
                                    <input type="radio" name="group1" value="Milk" id="choo2"> <label for="choo2">Choix 2</label>
                                </span>
                            </div>
                            <div class="spacer"></div>
                            <button type="submit" class="btn btn-primary submit">Enregistrer les modifications</button>
                            <a href="#" class="btn btn-default cancel">Annuler</a>
                        </form>
                    </div>

                </div>
                <div class="col-md-3">
                    <div class="box-white smallPad">
                        <ul id="menumoncompte">
                            <li><a href="#" class="current">Informations profil</a></li>
                            <li><a href="#">Mes objectifs</a></li>
                            <li><a href="#">Abonnement</a></li>
                            <li><a href="#">Facturation</a></li>
                            <li><a href="#">E-mails de notification</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include("inc/footer.php"); ?>