<?php include("inc/head.php"); ?>
<body class="dashboard-formation">
<?php include("inc/header.php"); ?>
<div id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="title">Mes formations</h1>
                <p class="desc">
                    <a href="#" class="contactformateur"><img src="images/chatformateur.png" alt=""> Contacter le
                        formateur</a> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                    voluptate.
                </p>
                <a href="#" class="btntopright formations btntopright-second">
                    Catalogue des formations
                </a>
                <div class="filterstop">
                    <a href="#" class="active">En cours</a>
                    <a href="#">Terminées</a>
                </div>
            </div>
            <div class="col-md-8">

				<div class="box-white votreaccompagnement">
                    <h2>Votre accompagnement</h2>
                    <span class="title">Du 01/11/16 au 01/11/17</span>
                    <br>
                    <div class="levels">
                        <div class="avancement jaugeContainer">
                            <span class="jauge">
                                <span class="remplir vert" style="width:85%">312 jours restants</span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="box-white une-formation">
                    <div class="col-md-2">
                        <img class="icon" src="images/icon-word.png" alt="">
                    </div>
                    <div class="col-md-10 content">
                        <h2>Maitrîse de l’outil</h2>
                        <span class="logiciel">Microsoft Office 2015</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                            voluptate velit esse cillum dolore eu fugiat nulla.</p>
                        <div class="levels">
                            <div class="col-md-6 avancement jaugeContainer">
                                <strong>Avancement</strong>
                                <span class="jauge">
									<span class="remplir" style="width:35%">35%</span>
								</span>
                            </div>
                            <div class="col-md-6 niveau jaugeContainer">
                                <strong>Niveau</strong>
                                <span class="jauge">
									<!-- 0-3 Débutant 4-6 Intermédiaire 7-10 Expert -->
									<span class="remplir intermediaire">Intermédiaire</span>
								</span>
                            </div>
                        </div>
                        <div class="actions">
                            <div class="steps">
                                <a href="#" class="passed"><span>1</span><br> Passage du test d'évaluation</a>
                                <a href="#" class="current"><span>2</span><br> Suivi de la formation</a>
                                <a href="#"><span>3</span><br> Passage du test intermédiaire</a>
                                <a href="#"><span>4</span><br> Passage de la certification</a>
                            </div>
                            <a href="#" class="go"><span class="icon"></span><span class="text">Reprendre</span></a>
                            <a href="#" class="stats">Mes stats</a>
                            <a href="#" class="contact">Contacter mon formateur</a>
                        </div>
                    </div>
                </div>

                <div class="box-white une-formation">
                    <div class="col-md-2">
                        <img class="icon" src="images/icon-excel.png" alt="">
                    </div>
                    <div class="col-md-10 content">
                        <h2>Comment écrire en italique</h2>
                        <span class="logiciel" style="color:#1e7e4a">Microsoft Excel 2015</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                            voluptate velit esse cillum dolore eu fugiat nulla.</p>
                        <div class="levels">
                            <div class="col-md-6 avancement jaugeContainer">
                                <strong>Avancement</strong>
                                <span class="jauge">
									<span class="remplir" style="width:70%">70%</span>
								</span>
                            </div>
                            <div class="col-md-6 niveau jaugeContainer">
                                <strong>Niveau</strong>
                                <span class="jauge">
									<!-- 0-3 Débutant 4-6 Intermédiaire 7-10 Expert -->
									<span class="remplir debutant">Débutant</span>
								</span>
                            </div>
                        </div>
                        <div class="actions">
                            <div class="steps">
                                <a href="#" class="passed"><span>1</span><br> Passage du test d'évaluation</a>
                                <a href="#" class="passed"><span>2</span><br> Suivi de la formation</a>
                                <a href="#" class="current"><span>3</span><br> Passage du test intermédiaire</a>
                                <a href="#"><span>4</span><br> Passage de la certification</a>
                            </div>
                            <a href="#" class="go"><span class="icon"></span><span class="text">Reprendre</span></a>
                            <a href="#" class="stats">Mes stats</a>
                            <a href="#" class="contact">Contacter mon formateur</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">

                <div class="miniStats box-white">
                    <div class="graphs">
                        <strong>
                            <span class="legend tempsform"></span> Temps de formation <br>
                            <span class="legend tempsacco"></span> <span class="tempsacco">Temps d'accompagnement</span>
                        </strong>
                        <canvas id="canvasminiStats"></canvas>
                    </div>
                    <div class="stats">
                        <div class="left">
							<span class="title">
								Temps de formation total
							</span>
                            <span class="value">
								20 565 h
							</span>
                        </div>
                        <div class="right">
                            <select name="timerange" id="timerange">
                                <option value="1">Total</option>
                                <option value="1">7 derniers jours</option>
                            </select>
                        </div>
                        <div class="clear"></div>
                        <br>
                        <div class="sliderStats">
                            <div class="stat">
                                <span class="title">Taux d'avancement</span>
                                <span class="value up">25%</span>
                            </div>
                            <div class="stat">
                                <span class="title">Taux de réussite</span>
                                <span class="value up">92%</span>
                            </div>
                            <div class="stat">
                                <span class="title">Formations par mois</span>
                                <span class="value down">2.8</span>
                            </div>
                            <div class="stat">
                                <span class="title">Formations par mois</span>
                                <span class="value down">2.8</span>
                            </div>
                            <div class="stat">
                                <span class="title">Taux de réussite</span>
                                <span class="value up">92%</span>
                            </div>
                            <div class="stat">
                                <span class="title">Formations par mois</span>
                                <span class="value down">2.8</span>
                            </div>
                            <div class="stat">
                                <span class="title">Formations par mois</span>
                                <span class="value down">2.8</span>
                            </div>
                        </div>
                    </div>
                </div>

                <script>
                    var randomScalingFactor = function () {
                        return Math.round(Math.random() * 100)
                    };
                    var lineChartData = {
                        labels: ["JAN", "FEV", "MAR", "AVR", "MAI", "JUN", "JUL"],
                        datasets: [
                            {
                                strokeColor: "#0e6a35",
                                pointColor: "#0e6a35",
                                pointHighlightFill: "#fff",
                                pointHighlightStroke: "#dbb027",
                                data: [5, 15, 18, 20, 5, 10, 8],
                                scaleShowGridLines: false,
                                bezierCurve: false,
                                fillColor: "rgba(255,255,255,0)",
                                scaleGridLineColor: "rgba(255, 255, 255, 0.1)"
                            },
                            {
                                strokeColor: "#FFF",
                                pointColor: "#FFF",
                                pointHighlightFill: "#fff",
                                pointHighlightStroke: "rgba(220,220,220,1)",
                                data: [0, 10, 15, 12, 13, 20, 12],
                                scaleShowGridLines: false,
                                bezierCurve: false,
                                fillColor: "rgba(220,220,220,0)",
                                scaleGridLineColor: "rgba(255, 255, 255, 0.1)"
                            }
                        ]
                    };
                    window.onload = function () {
                        var ctx = document.getElementById("canvasminiStats").getContext("2d");
                        window.myLine = new Chart(ctx).Line(lineChartData, {
                            responsive: true,
                            pointDot: true,
                            scaleShowVerticalLines: false,
                            datasetStrokeWidth: 4,
                            bezierCurveTension: 0.4,
                            scaleGridLineColor: "rgba(255,255,255,.15)",
                            scaleFontColor: "#fff",
                            scaleFontFamily: "proxima-nova-soft",
                            pointLabelFontFamily: "proxima-nova-soft",
                            scaleLabel: function (label) {
                                return label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "h";
                            },
                            scaleLineColor: "rgba(255, 255, 255, 0.1)",
                            showTooltips: false

                        });
                    }
                </script>

                <div class="side-historique box-white">
                    <strong class="title">
                        Historique opérationnel
                    </strong>
                    <div class="listHistorique">
                        <div class="story">
                            <div class="left">
								<span class="story-icon" style="background: #8443a3">
									<img src="images/icon-forum.png" alt="">
								</span>
                            </div>
                            <div class="right">
                                <time datetime="2012-02-11">11 fév. 2012</time>
                                <span class="story-title">Début d'une formation bla bla</span>
                                <span class="story-tell">Comment créer un sujet sur un selecteur de référence</span>
                            </div>
                        </div>
                        <div class="story">
                            <div class="left">
								<span class="story-icon" style="background: #8443a3">
									<img src="images/icon-forum.png" alt="">
								</span>
                            </div>
                            <div class="right">
                                <time datetime="2012-02-11">11 fév. 2012</time>
                                <span class="story-title">Création d’un sujet</span>
                                <span class="story-tell">Comment créer un sujet sur un selecteur de référence</span>
                            </div>
                        </div>
                        <div class="story message">
                            <div class="left">
								<span class="story-icon" style="">
									<img src="images/avatar.png" alt="">
								</span>
                            </div>
                            <div class="right">
                                <time datetime="2012-02-11">11 fév. 2012</time>
                                <span class="story-title"><span class="">Benoit</span></span>
                                <span class="story-tell">Hay salut Thibaut ! Comment s'est passé ta journée ?</span>
                            </div>
                        </div>
                        <div class="story">
                            <div class="left">
								<span class="story-icon" style="background: #ee8036">
									<img src="images/etoiletool.png" alt="">
								</span>
                            </div>
                            <div class="right">
                                <time datetime="2012-02-11">11 fév. 2012</time>
                                <span class="story-title">Ajout en favoris</span>
                                <span class="story-tell">Formation Word 1987</span>
                            </div>
                        </div>
                        <div class="story">
                            <div class="left">
								<span class="story-icon" style="background: #8443a3">
									<img src="images/icon-formations.png" alt="">
								</span>
                            </div>
                            <div class="right">
                                <time datetime="2012-02-11">11 fév. 2012</time>
                                <span class="story-title">Formation terminée</span>
                                <span class="story-tell">PowerPoint 2012 - Niveau 1</span>
                            </div>
                        </div>
                        <div class="story">
                            <div class="left">
								<span class="story-icon" style="background: #8443a3">
									<img src="images/icon-forum.png" alt="">
								</span>
                            </div>
                            <div class="right">
                                <time datetime="2012-02-11">11 fév. 2012</time>
                                <span class="story-title">Création d’un sujet</span>
                                <span class="story-tell">Comment créer un sujet sur un selecteur de référence</span>
                            </div>
                        </div>
                        <div class="story">
                            <div class="left">
								<span class="story-icon" style="background: #8443a3">
									<img src="images/icon-forum.png" alt="">
								</span>
                            </div>
                            <div class="right">
                                <time datetime="2012-02-11">11 fév. 2012</time>
                                <span class="story-title">Création d’un sujet</span>
                                <span class="story-tell">Comment créer un sujet sur un selecteur de référence</span>
                            </div>
                        </div>
                        <div class="story message">
                            <div class="left">
								<span class="story-icon" style="">
									<img src="images/avatar.png" alt="">
								</span>
                            </div>
                            <div class="right">
                                <time datetime="2012-02-11">11 fév. 2012</time>
                                <span class="story-title"><span class="">Benoit</span></span>
                                <span class="story-tell">Hay salut Thibaut ! Comment s'est passé ta journée ?</span>
                            </div>
                        </div>
                        <div class="story">
                            <div class="left">
								<span class="story-icon" style="background: #ee8036">
									<img src="images/etoiletool.png" alt="">
								</span>
                            </div>
                            <div class="right">
                                <time datetime="2012-02-11">11 fév. 2012</time>
                                <span class="story-title">Ajout en favoris</span>
                                <span class="story-tell">Formation Word 1987</span>
                            </div>
                        </div>
                        <div class="story">
                            <div class="left">
								<span class="story-icon" style="background: #8443a3">
									<img src="images/icon-formations.png" alt="">
								</span>
                            </div>
                            <div class="right">
                                <time datetime="2012-02-11">11 fév. 2012</time>
                                <span class="story-title">Formation terminée</span>
                                <span class="story-tell">PowerPoint 2012 - Niveau 1</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

			<?php /* <div class="col-md-3 formationAlaUne">
				<h3><span class="glyphicon glyphicon-heart" aria-hidden="true"></span> Formations à la une</h3>
				<div class="box-white une-formation">
					<div class="col-md-12">
						<img class="icon" src="images/icon-excel.png" alt="">
						<h2>Comment écrire en italique</h2>
						<span class="logiciel" style="color:#1e7e4a">Microsoft Excel 2015</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
						<div class="levels">
							<span class="niveau"><span class="icon debutant"></span> Débutant</span>
						</div>
						<!--<div class="actions">
							<a href="#" class="go"><span class="icon"></span><span class="text">Go</span></a>
						</div>-->
					</div>
				</div>
				<div class="box-white une-formation">
					<div class="col-md-12">
						<img class="icon" src="images/icon-word.png" alt="">
						<h2>Comment faire une tableau</h2>
						<span class="logiciel">Microsoft Word 2015</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
						<div class="levels">
							<span class="niveau"><span class="icon intermediaire"></span> Intermédiaire</span>
						</div>
						<!--<div class="actions">
							<a href="#" class="go"><span class="icon"></span><span class="text">Go</span></a>
						</div>-->
					</div>
				</div>
			</div> */ ?>
        </div>
    </div>
</div>
<?php include("inc/footer.php"); ?>

