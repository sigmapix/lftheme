<header>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<a href="#">
					<img class="logo" src="images/logo.png" alt="">
				</a>
			</div>
			<div class="containerTabAccount floatright">
				<div class="TabAccount">
					<img src="images/avatar.png" alt="" class="avatar">
					<strong>Thibaut Lamanthe</strong>
					<span class="actions">
						<a href="#">Compte</a> | <a href="#">Déconnexion</a>
					</span>
				</div>
			</div>
			<div class="col-md-6 floatright">
				<div class="menu">
					<a href="#" class="formations">Formations</a>
					<a href="#" class="bibliotheque">Bibliothèque</a>
					<a href="#" class="forum">Forum</a>
					<a href="#" class="blog">Blog</a>
				</div>
			</div>
		</div>
	</div>
</header>