<div id="stickyRight">
	<div id="contacterFormateur">
		<div class="title">
			<p><img src="images/chatformateur.png" alt=""> Contacter le formateur</p>
		</div>
	</div>
	<div id="toolbox" class="">
		<div class="title">
			<p><img src="images/etoiletool.png" alt=""> Boite à outils</p>
			<span id="notifications">7</span>
		</div>
		<div class="content">
			<div class="sidebar">
				<a href="#" class="fav-formation current">
					<span class="notifications">4</span>
				</a>
				<a href="#" class="fav-biblio">
					<span class="notifications">2</span>
				</a>
				<a href="#" class="fav-forum"></a>
				<a href="#" class="fav-recherche">
					<span class="notifications">1</span>
				</a>
			</div>
			<div class="list-formation list current">
				<ul>
					<li>
						<span class="delete"><span class="glyphicon glyphicon-trash"></span></span>
						<span class="niveau"> <span class="icon debutant"></span> </span>
						<span class="avancementContainer"><span class="avancement"><?php $n = rand(5,95); $x = 5; echo round(($n+$x/2)/$x)*$x ; ?>%</span></span>
						<span class="tempspasse">Temps passé : 2h00</span>
						<span class="iconContainer">
							<img class="icon" src="images/icon-word.png" alt="">
							<span class="notificationPoint"></span>
						</span>
						<span class="titleContainer">
							<strong>Maitrise de l'outil</strong>
							<span>Microsoft Office 2015</span>
						</span>
					</li>
					<li>
						<span class="delete"><span class="glyphicon glyphicon-trash"></span></span>
						<span class="niveau"> <span class="icon debutant"></span> </span>
						<span class="avancementContainer"><span class="avancement complete">100%</span></span>

						<span class="tempspasse">Temps passé : 2h00</span>
						<span class="iconContainer">
							<img class="icon" src="images/icon-excel.png" alt="">
							<span class="notificationPoint"></span>
						</span>
						<span class="titleContainer">
							<strong>Maitrise de l'outil informatique</strong>
							<span>Microsoft Office 2015</span>
						</span>
					</li>
					<li>
						<span class="delete"><span class="glyphicon glyphicon-trash"></span></span>
						<span class="niveau"> <span class="icon debutant"></span> </span>
						<span class="avancementContainer"><span class="avancement"><?php $n = rand(5,95); $x = 5; echo round(($n+$x/2)/$x)*$x ; ?>%</span></span>
						<span class="tempspasse">Temps passé : 2h00</span>
						<span class="iconContainer">
							<img class="icon" src="images/icon-word.png" alt="">
							<span class="notificationPoint"></span>
						</span>
						<span class="titleContainer">
							<strong>Maitrise de l'outil</strong>
							<span>Microsoft Office 2015</span>
						</span>
					</li>
					<li>
						<span class="delete"><span class="glyphicon glyphicon-trash"></span></span>
						<span class="niveau"> <span class="icon debutant"></span> </span>
						<span class="avancementContainer"><span class="avancement"><?php $n = rand(5,95); $x = 5; echo round(($n+$x/2)/$x)*$x ; ?>%</span></span>
						<span class="tempspasse">Temps passé : 2h00</span>
						<span class="iconContainer">
							<img class="icon" src="images/icon-word.png" alt="">
							<span class="notificationPoint"></span>
						</span>
						<span class="titleContainer">
							<strong>Maitrise de l'outil</strong>
							<span>Microsoft Office 2015</span>
						</span>
					</li>
					<li>
						<span class="delete"><span class="glyphicon glyphicon-trash"></span></span>
						<span class="niveau"> <span class="icon debutant"></span> </span>
						<span class="avancementContainer"><span class="avancement"><?php $n = rand(5,95); $x = 5; echo round(($n+$x/2)/$x)*$x ; ?>%</span></span>
						<span class="tempspasse">Temps passé : 2h00</span>
						<span class="iconContainer">
							<img class="icon" src="images/icon-word.png" alt="">
						</span>
						<span class="titleContainer">
							<strong>Maitrise de l'outil</strong>
							<span>Microsoft Office 2015</span>
						</span>
					</li>
					<li>
						<span class="delete"><span class="glyphicon glyphicon-trash"></span></span>
						<span class="niveau"> <span class="icon debutant"></span> </span>
						<span class="avancementContainer"><span class="avancement"><?php $n = rand(5,95); $x = 5; echo round(($n+$x/2)/$x)*$x ; ?>%</span></span>
						<span class="tempspasse">Temps passé : 2h00</span>
						<span class="iconContainer">
							<img class="icon" src="images/icon-word.png" alt="">
						</span>
						<span class="titleContainer">
							<strong>Maitrise de l'outil</strong>
							<span>Microsoft Office 2015</span>
						</span>
					</li>
					<li>
						<span class="delete"><span class="glyphicon glyphicon-trash"></span></span>
						<span class="niveau"> <span class="icon debutant"></span> </span>
						<span class="avancementContainer"><span class="avancement"><?php $n = rand(5,95); $x = 5; echo round(($n+$x/2)/$x)*$x ; ?>%</span></span>
						<span class="tempspasse">Temps passé : 2h00</span>
						<span class="iconContainer">
							<img class="icon" src="images/icon-excel.png" alt="">
						</span>
						<span class="titleContainer">
							<strong>Maitrise de l'outil</strong>
							<span>Microsoft Office 2015</span>
						</span>
					</li>
					<li>
						<span class="delete"><span class="glyphicon glyphicon-trash"></span></span>
						<span class="niveau"> <span class="icon debutant"></span> </span>
						<span class="avancementContainer"><span class="avancement"><?php $n = rand(5,95); $x = 5; echo round(($n+$x/2)/$x)*$x ; ?>%</span></span>
						<span class="tempspasse">Temps passé : 2h00</span>
						<span class="iconContainer">
							<img class="icon" src="images/icon-excel.png" alt="">
						</span>
						<span class="titleContainer">
							<strong>Maitrise de l'outil</strong>
							<span>Microsoft Office 2015</span>
						</span>
					</li>
					<li>
						<span class="delete"><span class="glyphicon glyphicon-trash"></span></span>
						<span class="niveau"> <span class="icon debutant"></span> </span>
						<span class="avancementContainer"><span class="avancement"><?php $n = rand(5,95); $x = 5; echo round(($n+$x/2)/$x)*$x ; ?>%</span></span>
						<span class="tempspasse">Temps passé : 2h00</span>
						<span class="iconContainer">
							<img class="icon" src="images/icon-word.png" alt="">
						</span>
						<span class="titleContainer">
							<strong>Maitrise de l'outil</strong>
							<span>Microsoft Office 2015</span>
						</span>
					</li>

				</ul>
			</div>
			<div class="list-biblio list">
				<ul>
					<li>
						<span class="delete"><span class="glyphicon glyphicon-trash"></span></span>
						<span class="niveau"> <span class="icon debutant"></span> </span>
						<span class="avancementContainer"><span class="download"><span class="icon"></span></span></span>
						<span class="tempspasse">Temps passé : 2h00</span>
						<span class="iconContainer">
							<img class="icon" src="images/icon-word.png" alt="">
							<span class="notificationPoint"></span>
						</span>
						<span class="titleContainer">
							<strong>Les raccourcis utiles à connaître en 10 étapes</strong>
							<span>Microsoft Office 2015 • Typo • Majuscule</span>
						</span>
					</li>
					<li>
						<span class="delete"><span class="glyphicon glyphicon-trash"></span></span>
						<span class="niveau"> <span class="icon debutant"></span> </span>
						<span class="avancementContainer"><span class="download"><span class="icon"></span></span></span>
						<span class="tempspasse">Temps passé : 2h00</span>
						<span class="iconContainer">
							<img class="icon" src="images/icon-word.png" alt="">
							<span class="notificationPoint"></span>
						</span>
						<span class="titleContainer">
							<strong>Les raccourcis utiles à connaître en 10 étapes</strong>
							<span>Microsoft Office 2015 • Typo • Majuscule</span>
						</span>
					</li>
					<li>
						<span class="delete"><span class="glyphicon glyphicon-trash"></span></span>
						<span class="niveau"> <span class="icon debutant"></span> </span>
						<span class="avancementContainer"><span class="download"><span class="icon"></span></span></span>
						<span class="tempspasse">Temps passé : 2h00</span>
						<span class="iconContainer">
							<img class="icon" src="images/icon-word.png" alt="">
						</span>
						<span class="titleContainer">
							<strong>Les raccourcis utiles à connaître en 10 étapes</strong>
							<span>Microsoft Office 2015 • Typo • Majuscule</span>
						</span>
					</li>
					<li>
						<span class="delete"><span class="glyphicon glyphicon-trash"></span></span>
						<span class="niveau"> <span class="icon expert"></span> </span>
						<span class="avancementContainer"><span class="download"><span class="icon"></span></span></span>
						<span class="tempspasse">Temps passé : 2h00</span>
						<span class="iconContainer">
							<img class="icon" src="images/icon-word.png" alt="">
						</span>
						<span class="titleContainer">
							<strong>Les raccourcis utiles à connaître en 10 étapes</strong>
							<span>Microsoft Office 2015 • Typo • Majuscule</span>
						</span>
					</li>
				</ul>
			</div>
			<div class="list-forum list">
				<ul>
					<li>
						<span class="delete"><span class="glyphicon glyphicon-trash"></span></span>
						<span class="avancementContainer"><span class="avancement forum ok">8</span></span>
						<strong>Maitrise de l'outil</strong>
						<span>Microsoft Office 2015</span>
					</li>
					<li>
						<span class="delete"><span class="glyphicon glyphicon-trash"></span></span>
						<span class="avancementContainer"><span class="avancement forum ok">8</span></span>
						<strong>Maitrise de l'outil</strong>
						<span>Microsoft Office 2015</span>
					</li>
					<li>
						<span class="delete"><span class="glyphicon glyphicon-trash"></span></span>
						<span class="avancementContainer"><span class="avancement forum ok">8</span></span>
						<strong>Maitrise de l'outil</strong>
						<span>Microsoft Office 2015</span>
					</li>
					<li>
						<span class="delete"><span class="glyphicon glyphicon-trash"></span></span>
						<span class="avancementContainer"><span class="avancement forum ok">8</span></span>
						<strong>Maitrise de l'outil</strong>
						<span>Microsoft Office 2015</span>
					</li>
					<li>
						<span class="delete"><span class="glyphicon glyphicon-trash"></span></span>
						<span class="avancementContainer"><span class="avancement forum ok">8</span></span>
						<strong>Maitrise de l'outil</strong>
						<span>Microsoft Office 2015</span>
					</li>
				</ul>
			</div>
			<div class="list-recherche list">
				<ul>
					<li>
						<span class="notificationPoint"></span>
						<span class="avancement search"></span>
						<strong>Ma recherche 1</strong>
						<span>Forum</span>
					</li>
					<li>
						<span class="avancement search"></span>
						<strong>Ma recherche 2</strong>
						<span>Forum</span>
					</li>
					<li>
						<span class="avancement search"></span>
						<strong>Ma recherche 3</strong>
						<span>Bibliothèque</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<?php

	if (isset($_GET["posttopic"])){
		if ($_GET["step"] == 1){
			?>
			<div class="modal fade" id="posttopic" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">Créer un nouveau sujet</h4>
						</div>
						<div class="modal-body">
							<p class="text-info"><i class="glyphicon glyphicon-bell"></i> Attention : Tout message ne respectant pas la charte ne sera pas validé </p>
							<form>
								<div class="form-group">
									<label for="field-question" class="control-label">Votre question :</label>
									<input type="text" class="form-control" id="field-question" required>
								</div>
								<div class="form-group">
									<label for="field-message" class="control-label">Message :</label>
									<textarea class="form-control" id="field-message" required></textarea>
								</div>
								<div class="form-group">
									<label for="field-tags" class="control-label">Tags :</label>
									<input type="text" class="form-control" id="field-tags" placeholder="Commencez à taper...">
								</div>
								<div class="row">
									<div class="form-group col-md-6">
										<label for="field-logiciel" class="control-label">Logiciel :</label>
										<select class="form-control" id="field-logiciel" required>
											<option>Selectionnez...</option>
										</select>
									</div>
									<div class="form-group col-md-6">
										<label for="field-version" class="control-label">Version :</label>
										<select class="form-control" id="field-version" required>
											<option>Selectionnez...</option>
										</select>
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
							<button type="submit" class="btn btn-primary">Poser ma question</button>
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
			<?php
		}

	}

?>

<div class="modal fade statsFormation" id="statsFormation" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Vos statistiques</h4>
			</div>
			<div class="modal-body une-formation">
				<div class="row">
					<div class="col-md-2 iconcontainer">
						<img class="icon" src="images/icon-word.png" alt="">
					</div>
					<div class="col-md-10 content">
						<h2>Maitrîse de l’outil</h2>
						<span class="logiciel">Microsoft Office 2015</span>
					</div>
				</div>
				<hr>
				<div id="statsContainer" style="background: #29569a;">
					<strong>Temps passé sur la formation</strong>
<!--					<span class="mois"><a href="#" class="prev">←</a> <span>Mars 2016</span> <a href="#" class="next">→</a></span>
-->					<canvas id="canvasStatsFormation"></canvas>
				</div>
				<div id="donutContainer" class="donutContainer">
					<strong>Taux de <br>réussite</strong>
					<span class="value">
						65%
					</span>
					<canvas id="chart-area" width="120"/>
				</div>
				<div class="clear"></div>
				<br>
				<div class="levels">
					<div class="col-md-6 avancement jaugeContainer">
						<strong>Avancement</strong>
						<span class="jauge">
							<span class="remplir" style="width:35%">35%</span>
						</span>
					</div>
					<div class="col-md-6 niveau jaugeContainer">
						<strong>Temps passé au total</strong>
						<span class="jauge">
							<!-- 0-3 Débutant 4-6 Intermédiaire 7-10 Expert -->
							<span class="remplir">11 jours et 3 heures</span>
						</span>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<a href="#"><img class="logo" src="images/logofooter.png" alt=""></a>
				© 2015 Copyright live-formation.com. Tous droits réservés. <br>
				<a href="#">Conditions générales</a> • <a href="#">Mentions légales</a> • <a href="#">Blog</a> • <a href="#">Contact</a>
			</div>
		</div>
	</div>
</footer>


<script type="text/javascript" src="js/min/jquery-1.12.0.min.js"> </script>
<script type="text/javascript" src="js/min/Chart.min.js"> </script>
<script type="text/javascript" src="js/min/jquery.bxslider.min.js"> </script>
<script type="text/javascript" src="js/min/custom-min.js"> </script>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
--><script src="js/bootstrap.min.js"></script>
</body>
</html>