<?php include("inc/head.php"); ?>

<body id="login">

<div class="container">

	<img class="logo" src="images/logo.png" alt="">

	<form class="form-signin box-white">
		<h2 class="form-signin-heading">Connectez-vous</h2>
		<label for="inputEmail" class="sr-only">Adresse e-mail</label>
		<input type="email" id="inputEmail" class="form-control" placeholder="Adresse e-mail" required autofocus>
		<label for="inputPassword" class="sr-only">Mot de passe</label>
		<input type="password" id="inputPassword" class="form-control" placeholder="Mot de passe" required>
		<!--<div class="checkbox">
			<input type="checkbox" value="remember-me" id="remember-me"> <label for="remember-me">Se souvenir de moi</label>
		</div>-->
		<button class="btn btn-lg btn-primary btn-block" type="submit">Connexion</button>
		<a href="#" class="forget">Mot de passe oublié ?</a>
	</form>

</div> <!-- /container -->

</body>
</html>
